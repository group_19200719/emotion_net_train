import os
from pathlib import Path

from dotenv import find_dotenv
from sklearn.model_selection import train_test_split

from . import DatasetClass, EmotionNetTrain


def chdir_to_projects_root():
    project_dir = Path(find_dotenv()).parent
    os.chdir(project_dir)


def write_labels_to_txt(class_list, folder):
    filename = "classes_labels.txt"
    filepath = os.path.join(folder, filename)

    with open(filepath, "w") as outfile:
        outfile.write("\n".join(class_list))


def get_datamodule(cfg):
    df_train = DatasetClass.read_and_preprocess_annotation(
        cfg["path2datset"], cfg["rel_path2_train"]
    )
    df_test = DatasetClass.read_and_preprocess_annotation(
        cfg["path2datset"], cfg["rel_path2_test"]
    )

    # опр. число различных эмоций в датасете
    emotion_num = DatasetClass.df_get_classes(df_train)
    cfg["emotion_num"] = emotion_num

    if cfg["debug"]:
        df_train = train_test_split(
            df_train,
            test_size=cfg["debug_samples"],
            stratify=df_train[cfg["target_col"]],
            random_state=42,
        )[1]

        df_test = train_test_split(
            df_test,
            test_size=cfg["debug_samples"],
            stratify=df_test[cfg["target_col"]],
            random_state=42,
        )[1]

    # задаем mean и std для предобработки входных изображений
    # от датасета на котором они обучались
    if cfg["model_name"] in set(["resnet50"]):
        images_mean = [0.485, 0.456, 0.406]
        images_std = [0.229, 0.224, 0.225]
    else:
        images_mean = [0.5, 0.5, 0.5]
        images_std = [0.5, 0.5, 0.5]

    # создаем загрузчик данных
    dm = DatasetClass.ClassificationDataModule(
        df_train,
        df_test,
        df_test,
        cfg["img_path_col"],
        cfg["target_col"],
        cfg["rescale_size"],
        cfg["batch_size"],
        cfg["num_workers"],
        cfg["oversampling_flag"],
        cfg["oversample_rate"],
        images_mean,
        images_std,
    )

    return cfg, dm, df_train, df_test


def get_model(cfg):
    model = EmotionNetTrain.LitModel(
        cfg["model_name"],
        cfg["model_weights"],
        cfg["model_inp_shape"],
        cfg["emotion_num"],
        learning_rate=cfg["learning_rate"],
        fc_only=cfg["fc_only"],
    )

    return model
